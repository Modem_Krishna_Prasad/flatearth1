/*=========================================================================================
  File Name: store.js
  Description: Vuex store
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Vuex from 'vuex'
import feathersVuex from "feathers-vuex";
import feathersClient from "./feathers-client";
const { service,auth,FeathersVuex } = feathersVuex(feathersClient, {
  idField: "id",
  enableEvents: true
});


import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
// import feathersVuex from "feathers-vuex";
// import feathersClient from "../feathers-client";
// const { service, auth, FeathersVuex } = feathersVuex(feathersClient, {
//   idField: "id"
// });

Vue.use(Vuex)
Vue.use(FeathersVuex)


// import moduleTodo from './todo/moduleTodo.js'
// import moduleCalendar from './calendar/moduleCalendar.js'
// import moduleChat from './chat/moduleChat.js'
// import moduleEmail from './email/moduleEmail.js'
//import moduleAuth from './auth/moduleAuth.js'
//import moduleECommerce from './eCommerce/moduleECommerce.js'


export default new Vuex.Store({
  getters,
  mutations,
  state,
  actions,
  modules: {
    // todo: moduleTodo,
    // calendar: moduleCalendar,
    // chat: moduleChat,
    // email: moduleEmail,
    //auth: moduleAuth,
    //eCommerce: moduleECommerce
  },
  plugins: [
    auth({
      userService: '/users',
      path: '/authentication',
      entity: 'users'
    }),
    //service('/authmanagement', {}),
    service('users', {
      replaceItems: true,
      idField:"_id"
    })
  ]

})
