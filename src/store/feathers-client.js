// plugins/featers-client
import feathers from '@feathersjs/feathers'
import socketio from '@feathersjs/socketio-client'
import auth from '@feathersjs/authentication-client'
import io from 'socket.io-client'

import { CookieStorage } from 'cookie-storage'

let API_URL = 'https://app.bligson.com/api'

if (window.location.hostname === 'localhost') {
  API_URL = 'http://localhost:3030'
}

const socket = io(API_URL, {
  transports: ['websocket']
})

const storage = new CookieStorage()

const feathersClient = feathers()
  .configure(socketio(socket))
  .configure(auth({ storage }))

export default feathersClient
