/*=========================================================================================
  File Name: vue.config.js
  Description: configuration file of vue
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


module.exports = {
  publicPath: '/',
  transpileDependencies: [
    'vue-echarts',
    'resize-detector'
  ],
  configureWebpack: {
    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    }
  },

  devServer: {
    clientLogLevel: "warning",
    hot: true,
    contentBase: "dist",
    compress: true,
    open: true,
    overlay: {warnings: false, errors: true},
    publicPath: "/",
    quiet: true,
    watchOptions: {
      poll: false,
      ignored: /node_modules/
    },
    proxy: {
      "/api":{
        target:"http://localhost:3030",
        ws: true,
        // proxyTimeout: 1000 * 60 * 10,
        // timeout: 1000 * 60 * 10
      },
      "/socket.io": {
         //secure:false,
       target:"ws://localhost:3030",
    //  target: 'ws://192.168.1.119:3030',
        ws: true,
        // proxyTimeout: 1000 * 60 * 10,
        // timeout: 1000 * 60 * 10
      }
    },
  },
  // devServer: {
  //   overlay: {
  //     warnings: true,
  //     errors: true
  //   }
  // }
}
